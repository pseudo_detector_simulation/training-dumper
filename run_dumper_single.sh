PATH_TO_INPUT=~/Projects/pseudo_detector/tutorial/detector_simulation/output/
PATH_TO_OUTPUT=~/Projects/pseudo_detector/tutorial/detector_simulation/output/

MODE=signal
#MODE=background

x=$(find $PATH_TO_INPUT -name "*qg*.root")
ITER=0
for i in $x; do # Not recommended, will break on whitespace
	INPUT=$i
	OUTPUT=$i".csv"
	echo "Executing ./bin/dumper --input $INPUT --output $OUTPUT --$MODE"
	./bin/dumper --input $INPUT --output $OUTPUT --$MODE > log$ITER &
	ITER=$(expr $ITER + 1)
done
