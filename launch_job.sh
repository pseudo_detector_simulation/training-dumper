#!/bin/bash
#SBATCH --partition=cpu-generic
#SBATCH --ntasks=1
#SBATCH --time=90:00:00
#SBATCH --mem=15000
#SBATCH --job-name="minbias_production"
#SBATCH --output=minbias_production-srun.out

echo "Job started" 
bash run_minbias_prod.sh
echo "All Done!"

