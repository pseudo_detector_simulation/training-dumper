FILENAME=MinBias.root
DIR=/storage/DSIP/physics/pseudo_detector
udocker run -v /home/adiluca/Projects/pseudo_detector/training-dumper:/home/temp -v $DIR:/home/out dumper_udocker /bin/bash -c "DelphesPythia8 /home/temp/delphes_cards/detectors/converter_card.tcl /home/temp/delphes_cards/pythia8/generatePileUp.cmnd /home/out/${FILENAME}"