# INPUT=data/hardqcd_10k_btag1.root
INPUT=delphes.root
OUTPUT=./out_sig.csv
MODE=signal

#MODE=background


#singularity exec docker://gitlab-registry.cern.ch/pseudo_detector_simulation/training-dumper/tagger-cpu:latest training_dumper/bin/dumper --input $INPUT --output $OUTPUT --$MODE

udocker run -v /home/adiluca/Projects/pseudo_detector/training-dumper:/home/temp dumper_udocker /bin/bash -c "cd training_dumper; ./bin/dumper --input /home/temp/${INPUT} --output /home/temp/${OUTPUT} --${MODE}"
