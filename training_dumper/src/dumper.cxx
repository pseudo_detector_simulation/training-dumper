#if defined(_MSC_VER)
#define RaveDllExport __declspec(dllexport)
#else
#define RaveDllExport
#endif

#include <iostream>
#include <fstream>
#include <iomanip>
#include <utility>
#include <vector>

#include "TROOT.h"
#include "TSystem.h"
#include "TCanvas.h"
#include "TApplication.h"

#include "TString.h"
#include "TMath.h"

#include "TH2.h"
#include "TH1.h"
#include "TSystem.h"
#include "TImage.h"
#include "THStack.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "TClonesArray.h"
#include "TLorentzVector.h"
#include "TVector3.h"

#include "classes/DelphesClasses.h"
#include "ExRootAnalysis/ExRootTreeReader.h"
#include "ExRootAnalysis/ExRootResult.h"

#include <rave/Version.h>
#include <rave/TransientTrackKinematicParticle.h>
//#include "RaveBase/Converters/interface/RaveStreamers.h"
#include <rave/KinematicConstraintBuilder.h>
#include <rave/KinematicTreeFactory.h>
#include <rave/VertexFactory.h>
//#include <rave/PerigeeCovariance3D.h>
//#include <rave/BasicTrack.h>
#include <rave/Track.h>
const double M_PION = 139.57e-3; // in GeV
const double M_PION2 = M_PION * M_PION;

//------------------------------------------------------------------------------
using namespace std;

bool Contained(TVector3 fatjet, TVector3 jet)
{
   bool contained = false;
   double DR = fatjet.DeltaR(jet);
   if (DR < 0.8)
      contained = true;
   return contained;
}
bool close_to_H(TVector3 fatjet, TVector3 h_jet)
{
   bool close = false;
   double DR = fatjet.DeltaR(h_jet);
   cout << "DR" << DR << endl;
   if (DR < 0.1)
      close = true;
   return close;
}

std::vector<Track *> SelectTracksInJet(Jet *jet, TClonesArray *branchEFlowTrack)
{
   // loop over all input tracks
   std::vector<Track *> tracks;
   TLorentzVector jetMomentum;
   jetMomentum.SetPtEtaPhiM(jet->PT, jet->Eta, jet->Phi, jet->Mass);
   for (int i_track = 0; i_track < branchEFlowTrack->GetEntriesFast(); i_track++)
   {
      Track *track = (Track *)branchEFlowTrack->At(i_track);
      TLorentzVector trkMomentum;
      trkMomentum.SetXYZT(track->X, track->Y, track->Z, track->T);
      ;

      double dr = jetMomentum.DeltaR(trkMomentum);
      double tpt = trkMomentum.Pt();
      double dxy = std::abs(track->D0);
      if (tpt == 0)
         continue;
      if (dr > 0.4)
         continue;
      // if (dxy > 6)
      //    continue;

      tracks.push_back(track);
      // cout << "Track PID contained" << track->PID << endl;
      // cout << "Track X: " << track->X << " , Track Y: " << track->Y << " , Track Z: " << track->Z << endl;
      // cout << "Track D0: " << track->D0 << " , Track phi: " << track->Phi << endl;
   }
   return tracks;
}


bool FatJet_selected(TLorentzVector fatjet)
{
   bool selected = false;

   if ((fatjet.Pt() > 400) && (fatjet.M() > 60))
      selected = true;
   return selected;
}

char *getCmdOption(char **begin, char **end, const std::string &option)
{
   char **itr = std::find(begin, end, option);
   if (itr != end && ++itr != end)
   {
      return *itr;
   }
   return 0;
}

bool cmdOptionExists(char **begin, char **end, const std::string &option)
{
   return std::find(begin, end, option) != end;
}

int main(int argc, char *argv[])
{
   int mode_ = -99;
   char *input_f = getCmdOption(argv, argv + argc, "--input");
   if (!input_f)
   {
      cout << "ERROR: Missing input file." << endl;
      return 1;
   }

   char *output_f = getCmdOption(argv, argv + argc, "--output");
   if (!output_f)
   {
      cout << "ERROR: Missing output file." << endl;
      return 1;
   }

   TChain *chain = new TChain("Delphes");
   chain->Add(input_f);
   if (chain == 0)
   {
      // if we cannot open the file, print an error message and return immediatly
      printf("Error: cannot open file\n");
      return 1;
   }

   if (cmdOptionExists(argv, argv + argc, "--signal"))
   {
      mode_ = 25;
      printf("Signal mode activated\n");
   }

   if (cmdOptionExists(argv, argv + argc, "--background"))
   {
      mode_ = 99;
      printf("Backgorund mode activated\n");
   }

   if (mode_ == -99)
   {
      printf("Error: no mode chosen.\n");
      return 1;
   }

   gSystem->Load("libDelphes");

   ExRootTreeReader *treeReader = new ExRootTreeReader(chain);
   ExRootResult *result = new ExRootResult();

   TClonesArray *branchFatJet = treeReader->UseBranch("FatJet");
   TClonesArray *branchVRTrackJet = treeReader->UseBranch("VRTrackJet");
   TClonesArray *branchParticle = treeReader->UseBranch("Particle");
   TClonesArray *branchEFlowTrack = treeReader->UseBranch("EFlowTrack");
   TClonesArray *branchEFlowTower = treeReader->UseBranch("EFlowTower");
   TClonesArray *branchEFlowMuon = treeReader->UseBranch("EFlowMuon");

   Long64_t allEntries = treeReader->GetEntries();
   Int_t countervr_bf=0;
   Int_t countervr=0;
   Int_t total_jet=0;
   Int_t low_tracknumber=0;
   Int_t low_vertexnumber=0;
   cout << "** Chain contains " << allEntries << " events" << endl;
   //allEntries = 100;
   Track *track;

   Long64_t entry;
   Int_t i;

   float Bz = 2.0;
   rave::ConstantMagneticField mfield(0., 0., Bz);
   rave::VertexFactory factory(mfield, rave::VacuumPropagator(), "kalman", 0);
   std::vector<float> vertex_number_vec;
   // Loop over all events
   cout << "Event number:" << allEntries << endl;
   int jet_to_write = 2;

   std::ofstream myfile;
   myfile.open(output_f);
   myfile << "FatJet_PT"
          << " "
          << "FatJet_Eta"
          << " "
          << "FatJet_Phi"
          << " "
          << "FatJet_T"
          << " "
          << "FatJet_Angularity"
          << " "
          << "FatJet_PlanarFlow"
          << " "
          << "FatJet_c2"
          << " "
          << "FatJet_d2"
          << " "
          << "FatJet_KtDeltaR"
          << " "
          << "FatJet_Aplanarity"
          << " "
          << "FatJet_ZCut"
          << " "
          << "FatJet_Mass"
          << " "
          << "FatJet_DeltaEta"
          << " "
          << "FatJet_DeltaPhi"
          << " "
          << "FatJet_Charge"
          << " "
          << "FatJet_EhadOverEem"
          << " "
          << "FatJet_NCharged"
          << " "
          << "FatJet_NNeutrals"
          << " "
          << "FatJet_PTD"
          << " "
          << "FatJet_Tau[0]"
          << " "
          << "FatJet_Tau[1]"
          << " "
          << "FatJet_Tau[2]"
          << " "
          << "FatJet_Tau[3]"
          << " "
          << "FatJet_Tau[4]";
   cout << "here" << endl;
   //subjet variables
   for (int jet_id = 0; jet_id < jet_to_write; jet_id++)
   {
      myfile << " "
             << "VRTrackJet_PT_" << jet_id << " "
             << "VRTrackJet_Eta_" << jet_id << " "
             << "VRTrackJet_Phi_" << jet_id << " "
             << "VRTrackJet_T_" << jet_id << " "
             << "VRTrackJet_Angularity_" << jet_id << " "
             << "VRTrackJet_PlanarFlow_" << jet_id << " "
             << "VRTrackJet_c2_" << jet_id << " "
             << "VRTrackJet_d2_" << jet_id << " "
             << "VRTrackJet_KtDeltaR_" << jet_id << " "
             << "VRTrackJet_Aplanarity_" << jet_id << " "
             << "VRTrackJet_ZCut_" << jet_id << " "
             << "VRTrackJet_Mass_" << jet_id << " "
             << "VRTrackJet_DeltaEta_" << jet_id << " "
             << "VRTrackJet_DeltaPhi_" << jet_id << " "
             << "VRTrackJet_Flavor_" << jet_id << " "
             << "VRTrackJet_BTag_" << jet_id << " "
             << "VRTrackJet_Charge_" << jet_id << " "
             << "VRTrackJet_EhadOverEem_" << jet_id << " "
             << "VRTrackJet_secvtx_energy_" << jet_id << " "
             << "VRTrackJet_secvtx_mass_" << jet_id << " "
             << "VRTrackJet_secvtx_sig3D_" << jet_id << " "
             << "VRTrackJet_secvtx_Lxy_" << jet_id << " "
             << "VRTrackJet_secvtx_L3D_" << jet_id << " "
             << "VRTrackJet_secvtx_efrac_" << jet_id << " "
             << "VRTrackJet_secvtx_Ntracks_" << jet_id << " "
             << "VRTrackJet_secvtxnvtx2trk_" << jet_id;

      ;
   }
   myfile << "\n";

   for (entry = 0; entry < allEntries; ++entry)
   {  cout << entry << endl;
      // Load selected branches with data from specified event
      treeReader->ReadEntry(entry);

      TVector3 higgs;
      if (mode_ == 25)
      {
         std::vector<int> higgs_idx;
         for (int i_particle = 0; i_particle < branchParticle->GetEntriesFast(); i_particle++)
         {
            GenParticle *particle = (GenParticle *)branchParticle->At(i_particle);
            if ((particle->PID == 25) && (particle->Status == 62))
            {
               higgs_idx.push_back(i_particle);
            }
         }
         GenParticle *particle = (GenParticle *)branchParticle->At(higgs_idx.at(0));
         higgs.SetPtEtaPhi(particle->PT, particle->Eta, particle->Phi);
      }

      // Loop over all fatjets in event
      for (i = 0; i < branchFatJet->GetEntriesFast(); ++i)
      {
         Jet *fatjet_ = (Jet *)branchFatJet->At(i);
         cout << "Fatjet " << i << endl;
         vector<Jet *> vrjet_vec;
         vector<float> VRTrackJet_secvtx_energy;
         vector<float> VRTrackJet_secvtx_mass;
         vector<float> VRTrackJet_secvtx_sig3D;
         vector<float> VRTrackJet_secvtx_Lxy;
         vector<float> VRTrackJet_secvtx_L3D;
         vector<float> VRTrackJet_secvtx_efrac;
         vector<float> VRTrackJet_secvtx_Ntracks;
         vector<float> VRTrackJet_secvtxnvtx2trk;

         TLorentzVector fatjet_selection;
         fatjet_selection.SetPtEtaPhiM(fatjet_->PT, fatjet_->Eta, fatjet_->Phi, fatjet_->Mass);
         // if (!FatJet_selected(fatjet_selection))
         //    continue;

         std::vector<int> contained_jet_idx;
         TVector3 fatjet;
         fatjet.SetPtEtaPhi(fatjet_->PT, fatjet_->Eta, fatjet_->Phi);
         if (mode_ == 25)
         {
            if (!close_to_H(fatjet, higgs))
               continue;
            cout << "Found higgs fatjet" << endl;
         }
         

         for (int i_j = 0; i_j < branchVRTrackJet->GetEntriesFast(); i_j++)
         {
            Jet *vrjet = (Jet *)branchVRTrackJet->At(i_j);

            if (vrjet->PT == 0)
               continue;
            if (vrjet->PT < 20)
               continue;
            TVector3 jet;
            // if (Jet_PT[i_fj] < 629 || Jet_PT[i_fj] > 631) continue;
            jet.SetPtEtaPhi(vrjet->PT, vrjet->Eta, vrjet->Phi);
            cout << fatjet.DeltaR(jet) << endl;
            if (Contained(fatjet, jet))
            {
               contained_jet_idx.push_back(i_j);
            }
         }
         if (contained_jet_idx.size() < 2)
         {
            cout << "Only " << contained_jet_idx.size() << ",  not enough contained jet" << endl;
            countervr_bf++;
            continue;
         }
         cout << contained_jet_idx.size() <<"contained jet" << endl;
         for (int i_j = 0; i_j < contained_jet_idx.size(); i_j++)
         {
            float jet_track_energy = 0;
            Jet *vrjet = (Jet *)branchVRTrackJet->At(contained_jet_idx.at(i_j));

            // Loop over all jet's constituents
            vector<rave::Track> jet_tracks;

            std::vector<Track *> tracks = SelectTracksInJet(vrjet, branchEFlowTrack);
            cout << "Tracks in jet" << tracks.size() << endl;
            for (int i_trk = 0; i_trk < tracks.size(); i_trk++)
            {
               track = tracks[i_trk];

               double d0 = track->D0 * 0.1; //Delphes is in mm and rave is in cm
               double z0 = track->DZ * 0.1;
               double pt = track->PT;
               double phi = track->Phi;
               double ctth = track->CtgTheta;
               double q = track->Charge;

               // double x = track->X * 0.1;    //
               // double y = track->Y * 0.1;    //
               // double z = track->Z * 0.1;    //
               // double px = track->P4().Px(); //
               // double py = track->P4().Py(); //
               // double pz = track->P4().Pz(); //

               double x = d0*sin(phi);
               double y = - d0*cos(phi);
               double z = z0;
               double px = pt/q * cos(phi);
               double py = pt/q * sin(phi);
               double pz = pt/q * ctth;

               //  cout << "Storing to rave track" << endl;
               rave::Vector6D track6d(x, y, z, px, py, pz);
               //  cout << "Stored to rave track" << endl;

               double deld0 = track->ErrorD0 * 0.1; //in cm
               double delz0 = track->ErrorDZ * 0.1; //in cm
               double delpt = track->ErrorPT;       //in GeV/c
               double delphi = track->ErrorPhi;
               double delcotth = track->ErrorCtgTheta;
               double deltht = delcotth / (1 + ctth * ctth);

               double cov5ddiag[] = {deld0 * deld0, delz0 * delz0, delpt * delpt, delphi * delphi, deltht * deltht};

               double covd0d0 = cov5ddiag[0];
               double covz0z0 = cov5ddiag[1];
               double covptpt = cov5ddiag[2];
               double covphiphi = cov5ddiag[3];
               double covthth = cov5ddiag[4];

               double dpxpx = py * py * covphiphi + covptpt * cos(phi) * cos(phi) / (q * q);
               double dpxpy = -px * py * covphiphi + covptpt * cos(phi) * sin(phi) / (q * q);
               double dpxpz = covptpt * cos(phi) * ctth / (q * q); // + 0.00000001;
               double dxpx = -d0 * py * covphiphi * cos(phi);
               double dypx = -d0 * py * covphiphi * sin(phi);
               double dpypy = px * px * covphiphi + covptpt * sin(phi) * sin(phi) / (q * q);
               double dpypz = covptpt * sin(phi) * ctth / (q * q); // + 0.0000001;
               double dxpy = d0 * px * covphiphi * cos(phi);
               double dypy = d0 * px * covphiphi * sin(phi);
               double dypz = 0;
               double dxpz = 0;
               double dzpx = 0;
               double dzpy = 0;
               double dzpz = 0;
               double dpzpz = covptpt * ctth * ctth + pt * pt * covthth * pow((1 + ctth * ctth), 2) / (q * q);
               double dxx = d0 * d0 * cos(phi) * cos(phi) * covphiphi + covd0d0 * sin(phi) * sin(phi);
               double dxy = (-covd0d0 + d0 * d0 * covphiphi) * cos(phi) * sin(phi);
               double dxz = 0;
               double dyy = covd0d0 * cos(phi) * cos(phi) + d0 * d0 * sin(phi) * sin(phi) * covphiphi;
               double dyz = 0;
               double dzz = covz0z0;

               rave::Covariance6D cov6d(dxx, dxy, dxz,
                                        dyy, dyz, dzz,
                                        dxpx, dxpy, dxpz,
                                        dypx, dypy, dypz,
                                        dzpx, dzpy, dzpz,
                                        dpxpx, dpxpy, dpxpz,
                                        dpypy, dpypz, dpzpz);

               jet_tracks.push_back(rave::Track(track6d, cov6d, q, 0.0, 0.0));

            } //END loop over jet constituents

            if (jet_tracks.size() <= 1){
               low_tracknumber++;
               continue;
            }
               
            //   cout << jet_tracks.size() << endl;
            //   cout<<"Before creating factory" << jet_tracks.size()<<endl;
            jet_track_energy = 0;

            for (int i = 0; i < jet_tracks.size(); i++)
            {
               jet_track_energy += sqrt(jet_tracks[i].momentum().mag2() + M_PION2);
            }

            std::vector<rave::Vertex> rvertices;
            try
            {
               rvertices = factory.create(jet_tracks, "kalman"); //, "avr" );
            }
            catch (...)
            {
            }

            vertex_number_vec.push_back(rvertices.size());
            cout << "Number of vertex" << rvertices.size() << endl;
            if (rvertices.size() == 0){
               low_vertexnumber++;
               continue;
            }
            //for(int j_vtx = 0; j_vtx <rvertices.size(); j_vtx++ )
            vrjet_vec.push_back(vrjet);

            int nvtx2trk = 0;
            // cout << "number of vertices: " << rvertices.size() << endl;
            for (int j_vtx = 0; j_vtx < rvertices.size(); j_vtx++)
            {
               rave::Vertex vtx = rvertices.at(j_vtx);
               int n_vertex = -1;
               n_vertex = vtx.weightedTracks().size();
               // cout << "Number of tracks " << n_vertex << endl;
               if (n_vertex == 2)
               {
                  // cout << "Found" << endl;
                  nvtx2trk++;
               }
            }

            for (int j_vtx = 0; j_vtx < 1; j_vtx++)
            {
               rave::Vertex vtx = rvertices.at(j_vtx);
               int track_sec_vertex = -1;
               track_sec_vertex = vtx.weightedTracks().size();
               Double_t energy = -1;
               Double_t threshold = 0.5;
               for (const auto &wt_trk : vtx.weightedTracks())
               {
                  if (wt_trk.first > threshold)
                  {
                     energy += sqrt(wt_trk.second.momentum().mag2() + M_PION2);
                  }
               }

               double efrac_numerator = 0;

               for (const auto &wt_trk : vtx.weightedTracks())
               {
                  double energy_temp = sqrt(wt_trk.second.momentum().mag2() + M_PION2);
                  efrac_numerator += energy_temp;
               }
               double svx_efrac = efrac_numerator / jet_track_energy;

               // cout << "Energy: " << energy << endl;
               rave::Point3D sum_momentum(0, 0, 0);
               double sum_energy = 0;
               for (const auto &wt_trk : vtx.weightedTracks())
               {
                  if (wt_trk.first > threshold)
                  {
                     const rave::Vector3D &mom = wt_trk.second.momentum();
                     sum_momentum += mom;
                     sum_energy += sqrt(mom.mag2() + pow(M_PION, 2));
                  }
               }
               Double_t mass = sqrt(pow(sum_energy, 2) - sum_momentum.mag2());
               // cout << "Mass: " << mass << endl;

               double Lx = vtx.position().x();
               double Ly = vtx.position().y();
               double Lz = vtx.position().z();
               double decaylength = std::sqrt(Lx * Lx + Ly * Ly + Lz * Lz);
               // cout << "L3D: " << decaylength << endl;
               if (decaylength == 0)
                  continue; //return 0;

               const rave::Covariance3D &cov = vtx.error();
               double xhat = Lx / decaylength;
               double yhat = Ly / decaylength;
               double zhat = Lz / decaylength;
               double var =
                   xhat * xhat * cov.dxx() +
                   yhat * yhat * cov.dyy() +
                   zhat * zhat * cov.dzz() +
                   2. * xhat * yhat * cov.dxy() +
                   2. * xhat * zhat * cov.dxz() +
                   2. * yhat * zhat * cov.dyz();
               Double_t decay_length_variance = var;

               Double_t sig3D = -1;

               if (decaylength == 0)
                  sig3D = -1;
               double err = sqrt(decay_length_variance);
               sig3D = decaylength / err;
               VRTrackJet_secvtx_energy.push_back(energy);
               VRTrackJet_secvtx_mass.push_back(mass);
               VRTrackJet_secvtx_sig3D.push_back(sig3D);
               VRTrackJet_secvtx_Lxy.push_back(sqrt(Lx * Lx + Ly * Ly));
               VRTrackJet_secvtx_L3D.push_back(decaylength);
               VRTrackJet_secvtx_efrac.push_back(svx_efrac);
               VRTrackJet_secvtx_Ntracks.push_back(track_sec_vertex);
               VRTrackJet_secvtxnvtx2trk.push_back(nvtx2trk);
            }
         }
         if (vrjet_vec.size() < 2){
            cout << " not enough jet to be written " << endl;
            countervr++;
            continue;}
         if (vrjet_vec.size() < 3)
         {
            jet_to_write = vrjet_vec.size();
         }

         cout << "Start writing" << endl;
         //fatjet variables
         myfile << fatjet_->PT << " "
                << fatjet_->Eta << " "
                << fatjet_->Phi << " "
                << fatjet_->T << " "
                << fatjet_->Angularity << " "
                << fatjet_->PlanarFlow << " "
                << fatjet_->c2 << " "
                << fatjet_->d2 << " "
                << fatjet_->KtDeltaR << " "
                << fatjet_->Aplanarity << " "
                << fatjet_->ZCut << " "
                << fatjet_->Mass << " "
                << fatjet_->DeltaEta << " "
                << fatjet_->DeltaPhi << " "
                << fatjet_->Charge << " "
                << fatjet_->EhadOverEem << " "
                << fatjet_->NCharged << " "
                << fatjet_->NNeutrals << " "
                << fatjet_->PTD << " "
                << fatjet_->Tau[0] << " "
                << fatjet_->Tau[1] << " "
                << fatjet_->Tau[2] << " "
                << fatjet_->Tau[3] << " "
                << fatjet_->Tau[4];
         //subjet variables
         for (int jet_id = 0; jet_id < jet_to_write; jet_id++)
         {
            int i_j = jet_id;
            myfile << " "
                   << vrjet_vec[i_j]->PT << " "
                   << vrjet_vec[i_j]->Eta << " "
                   << vrjet_vec[i_j]->Phi << " "
                   << vrjet_vec[i_j]->T << " "
                   << vrjet_vec[i_j]->Angularity << " "
                   << vrjet_vec[i_j]->PlanarFlow << " "
                   << vrjet_vec[i_j]->c2 << " "
                   << vrjet_vec[i_j]->d2 << " "
                   << vrjet_vec[i_j]->KtDeltaR << " "
                   << vrjet_vec[i_j]->Aplanarity << " "
                   << vrjet_vec[i_j]->ZCut << " "
                   << vrjet_vec[i_j]->Mass << " "
                   << vrjet_vec[i_j]->DeltaEta << " "
                   << vrjet_vec[i_j]->DeltaPhi << " "
                   << vrjet_vec[i_j]->Flavor << " "
                   << vrjet_vec[i_j]->BTag << " "
                   << vrjet_vec[i_j]->Charge << " "
                   << vrjet_vec[i_j]->EhadOverEem << " "
                   << VRTrackJet_secvtx_energy[i_j] << " "
                   << VRTrackJet_secvtx_mass[i_j] << " "
                   << VRTrackJet_secvtx_sig3D[i_j] << " "
                   << VRTrackJet_secvtx_Lxy[i_j] << " "
                   << VRTrackJet_secvtx_L3D[i_j] << " "
                   << VRTrackJet_secvtx_efrac[i_j] << " "
                   << VRTrackJet_secvtx_Ntracks[i_j] << " "
                   << VRTrackJet_secvtxnvtx2trk[i_j];
         }
         myfile << "\n";
         total_jet++;
      } //END loop over jet
   }    //END loop over event
   myfile.close();
   cout << "Dropped before vx rec "<<countervr_bf << endl;
   cout << "Dropped after vx rec "<<countervr << endl;
   cout << "total jet  "<<total_jet << endl;
   cout << "low_tracknumber  "<<low_tracknumber << endl;
   cout << "low_vertexnumber" << low_vertexnumber << endl;
   cout << allEntries << endl;
   cout << "** Exiting..." << endl;
   delete treeReader;
   delete chain;
}
