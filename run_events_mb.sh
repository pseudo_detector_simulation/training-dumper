PYTHIA_CARD=/home/temp/delphes_cards/pythia8/hardQCD_400_pileup.cmd
DETECTOR_CARD=/home/temp/delphes_cards/detectors/delphes_card_ATLAS_fatjet_VRjet_vertex_tracksmearing_pileup.tcl
FILENAME=hardqcd_MinBias.root
DIR=/storage/DSIP/physics/pseudo_detector

udocker run -v /home/adiluca/Projects/pseudo_detector/training-dumper:/home/temp -v $DIR:/home/out dumper_udocker /bin/bash -c "DelphesPythia8 ${DETECTOR_CARD} ${PYTHIA_CARD} /home/out/${FILENAME}"