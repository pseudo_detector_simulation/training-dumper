# training-dumper
Dumper used for Delphes output developed for feature ranking studies.
## Local Docker image build
You need to have a working Docker engine installation working on your local machine. More info on how to have Docker engine working on your machine can be find [here](https://docs.docker.com/engine/install/).

After that, follow these steps:
1. Clone the repository on your machine:
```sh
$ git clone https://gitlab.cern.ch/pseudo_detector_simulation/training-dumper.git
```
2. Change directory to the home repository folder:
```sh
$ cd training-dumper
```
3. Build the Docker image:
```sh 
$ sudo docker build -t training_dumper .
```
You are now ready to run the dumper.
> You need to set the `-t` argurment in order to be able to refer to your Docker image in the `run_dumper.sh` code.
# Run the dumper 
An example file can be downloaded [here](https://cernbox.cern.ch/index.php/s/uzyDyWVecLOvpm1).

In the home repository folder, just type
```sh
$ sh run_dumper.sh
```
Modify the `run_dumper.sh` to set `INPUT`, `OUTPUT`, `MODE` and `DOCKER_TAG`.
# Run the dumper on HPC resources
## Build udocker image
1. Install udocker (https://github.com/indigo-dc/udocker/blob/master/doc/installation_manual.md)
2. udocker pull gitlab-registry.cern.ch/pseudo_detector_simulation/training-dumper/tagger-cpu:latest
3. udocker create --name=dumper_udocker gitlab-registry.cern.ch/pseudo_detector_simulation/training-dumper/tagger-cpu:latest
4.	udocker setup --execmode=S1 dumper_udocker
## Setup volumes to mount
Local volumes should be attached to udocker to process local files and to save output files. Remeber to correctly modify your path in the launch script. It's important to not modify files within the udocker image. All changes can be stashe by adding the --rm flag. In this case, you will need to build again the image. 
